import QtQuick 2.6
import QtQuick.Window 2.2
Window {
    id:mainWindow
    visible: true
    width: 488
    height: 400
    title: qsTr("Hello World")
    flags: Qt.FramelessWindowHint|Qt.Window
    TestLog{
        id:test_log
        visible: false}
    Login{
        onLoginclicked: {
            test_log.text=userName+passWord
            test_log.show()
            mainWindow.hide()
        }
    }
}
