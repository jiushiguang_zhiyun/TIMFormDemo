import QtQuick 2.0

Rectangle {
        id: rect
        width: parent.width * 0.6
        radius: parent.radius
        color: rect.state == "left"? "#4040FF" : "#CCCCCC"
        state: "left"
        anchors {
            top: parent.top
            bottom: parent.bottom
        }

        states: [
            State {
                name: "right"
                PropertyChanges {
                    target: rect
                    x: root.width - rect.width
                }
            }

        ]

        transitions: [
            Transition {
                from: "*"
                to: "*"
                NumberAnimation { property: "x"; duration: 200 }
            }
        ]

        Text {
            id: label
            anchors.centerIn: parent
            text: rect.state == "left"? root.leftString : root.rightString
            color: "white"
            font.pointSize: 10
        }
    }


