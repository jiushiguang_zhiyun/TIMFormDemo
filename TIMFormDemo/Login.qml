import QtQuick 2.0

Item {
    id:root
    width: 488
    height: 400
    signal loginclicked(string userName,string passWord)
    MouseArea { //为窗口添加鼠标事件
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton //只处理鼠标左键
        property point clickPos: "0,0"
        onPressed: { //接收鼠标按下事件
            clickPos  = Qt.point(mouse.x,mouse.y)
        }
        onPositionChanged: { //鼠标按下后改变位置
            //鼠标偏移量
            var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
            //如果mainwindow继承自QWidget,用setPos
            mainWindow.setX(mainWindow.x+delta.x)
            mainWindow.setY(mainWindow.y+delta.y)
        }
    }
    Image{
        anchors.fill: parent
        source: "image/background.png"
        sourceSize: Qt.size(parent.width,parent.height)
    }
    NamePassword{
        id:m_namepassword
        x:146
        y:225
        onControlTextChanged:{console.log(text)}
        onControl2TextChanged:{console.log(text)}
    }
    Text{
        text:qsTr("注册账号")
        anchors.left:m_namepassword.right
        anchors.leftMargin: 14
        anchors.top: m_namepassword.top
        anchors.topMargin: m_namepassword.height/8
        color:Qt.rgba(91/255,164/255,234/255,1)
        font.pointSize: 14
    }
    Text{
//        text:qsTr("找回密码")
        font.pointSize: 14
        anchors.left:m_namepassword.right
        anchors.leftMargin: 14
        anchors.top: m_namepassword.top
        anchors.topMargin: m_namepassword.height*5/8
        color:Qt.rgba(91/255,164/255,234/255,1)
        text: '<html></style><a href="http://baidu.com">找回密码</a></html>'
         onLinkActivated: Qt.openUrlExternally(link)

    }
    MyCheckBoxGroup{
        id:m_checkBoxGroup
        anchors.top:m_namepassword.bottom
        anchors.topMargin: 5
        anchors.left: m_namepassword.left
        anchors.right: m_namepassword.right
    }
    MyOpacityMaskImage{
        anchors.top: m_namepassword.top
        anchors.right: m_namepassword.left
        anchors.rightMargin: 10
    }
    MyEnterButton{
        width: m_namepassword.width
        height: 33
        anchors.left: m_namepassword.left
        anchors.bottom:  parent.bottom
        anchors.bottomMargin: 15
        onClicked: {
            console.log(m_namepassword.editText,m_namepassword.passWordText)
                root.loginclicked(m_namepassword.editText,m_namepassword.passWordText)
        }
    }

    Image{
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        source: "image/add.png"
    }
    Image{
        anchors.right:  parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        source: "image/qr.png"
    }
}
