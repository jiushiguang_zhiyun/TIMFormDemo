import QtQuick 2.0
import QtQuick.Controls 2.1

Rectangle{
    id:root
    property color borderColor : "#21be2b"
    property alias editText: edit.text
    property alias passWordText: control2.text
    signal controlTextChanged(string text)
    signal control2TextChanged(string text)

    width: 200
    height: 80
    radius: 5
    border.color: borderColor
    border.width: 1
    Column{
        id:m_column
        Flickable {
             id: flick

             width: root.width; height: root.height/2;
             contentWidth: edit.paintedWidth
             contentHeight: edit.paintedHeight
             clip: true

             function ensureVisible(r)
             {
                 if (contentX >= r.x)
                     contentX = r.x;
                 else if (contentX+width <= r.x+r.width)
                     contentX = r.x+r.width-width;
                 if (contentY >= r.y)
                     contentY = r.y;
                 else if (contentY+height <= r.y+r.height)
                     contentY = r.y+r.height-height;
             }

             TextEdit {
                 id: edit
                 width: flick.width
                 height: flick.height
                 verticalAlignment: Text.AlignVCenter
                 focus: true
                 wrapMode: TextEdit.Wrap
                 onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)

             }
             Text{
                 id:m_text
//                 width: flick.width
//                 height: flick.height
                 anchors.verticalCenter: edit.verticalCenter
                 text:edit.text==""?"显示当前文本,如MFLG等":""
//                 verticalAlignment: Text.AlignVCenter
             }
         }
        Canvas{
            y:parent.height/2;
            width: root.width;
            height: 1;
            onPaint: {
                var ctx = getContext("2d");
                ctx.moveTo(0,0);
                ctx.lineTo(width,height);
                ctx.lineWidth=height;
                ctx.strokeStyle = borderColor
                ctx.stroke();

            }
        }
        TextField {
            id: control2
            placeholderText: qsTr("密码")
            hoverEnabled: true
            background: Rectangle {
                implicitWidth: root.width
                implicitHeight: root.height/2
                color: "transparent"
            }
            onEditingFinished: {
                 root.control2TextChanged(text)
            }
        }

    }
}


