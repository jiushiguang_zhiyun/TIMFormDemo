import QtQuick 2.0
import QtGraphicalEffects 1.0
Item {
    width: 100
    height:100
    Image{
        id:m_icon
        source: "image/icon.jpg"
        sourceSize: Qt.size(parent.width,parent.height)
        visible: false
    }
    Rectangle{
        id: mask
        anchors.fill: m_icon
        radius: mask.height/2
        visible: false
    }
    OpacityMask {
        anchors.fill: m_icon
        source: m_icon
        maskSource: mask
    }
}
